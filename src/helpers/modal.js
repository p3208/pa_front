import Hub from "../event";

export default {
  display: function ({data, title, component, closable, event, callback}) {
    Hub.$emit("set-modal", data, title, component, closable);
    Hub.$emit("open-modal");
    Hub.$once(event, callback);
  },
};
