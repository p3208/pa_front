import i18n from "../locales";

export default {
  default: () => ({ valid: true, message: '' }),
  user: {
    firstName: (value) => {
      const inputValidator = { valid: false, message: "" };
      if (value.length < 2)
        inputValidator.message = i18n.t(
          "lang.authent.global.validation.firstName.min"
        );
      else inputValidator.valid = true;
      return inputValidator;
    },
    lastName: (value) => {
      const inputValidator = { valid: false, message: "" };
      if (value.length < 3)
        inputValidator.message = i18n.t(
          "lang.authent.global.validation.lastName.min"
        );
      else inputValidator.valid = true;
      return inputValidator;
    },
    email: (value) => {
      const inputValidator = { valid: false, message: "" };
      if (!value)
        inputValidator.message = i18n.t(
          "lang.authent.global.validation.email.required"
        );
      else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/i.test(value))
        inputValidator.message = i18n.t(
          "lang.authent.global.validation.email.invalid"
        );
      else inputValidator.valid = true;
      return inputValidator;
    },
    password: (value) => {
      const inputValidator = { valid: false, message: "" };
      if (!value)
        inputValidator.message = i18n.t(
          "lang.authent.global.validation.password.required"
        );
      else if (value.length < 6)
        inputValidator.message = i18n.t(
          "lang.authent.global.validation.password.min"
        );
      else inputValidator.valid = true;
      return inputValidator;
    },
  },
  mission: {
    title: (value) => {
      const inputValidator = { valid: false, message: "" };
      if (value.length < 2)
        inputValidator.message = i18n.t("lang.mission.global.validation.title.min");
      else if (value.length > 100) {
        inputValidator.message = i18n.t("lang.mission.global.validation.title.max");
      }
      else inputValidator.valid = true;
      return inputValidator;
    },
    description: (value) => {
      const inputValidator = { valid: false, message: "" };
      if (value.length < 10)
        inputValidator.message = i18n.t("lang.mission.global.validation.description.min");
      else if (value.length > 1000) {
        inputValidator.message = i18n.t("lang.mission.global.validation.description.max");
      }
      else inputValidator.valid = true;
      return inputValidator;
    },
    startDate: (value) => {
      const inputValidator = { valid: false, message: "" };
      if (!value)
        inputValidator.message = i18n.t("lang.mission.global.validation.startDate.required");
      else inputValidator.valid = true;
      return inputValidator;
    },
    endDate: (value) => {
      const inputValidator = { valid: false, message: "" };
      if (!value)
        inputValidator.message = i18n.t("lang.mission.global.validation.endDate.required");
      else if (new Date(value) < new Date()) {
        inputValidator.message = i18n.t("lang.mission.global.validation.endDate.invalid");
      } else inputValidator.valid = true;
      return inputValidator;
    },
    remuneration: (value) => {
      const inputValidator = { valid: false, message: "" };
      if (value < 0)
        inputValidator.message = i18n.t("lang.mission.global.validation.remuneration.min");
      else inputValidator.valid = true;
      return inputValidator;
    },
    // idStatus: () => {}, TODO Add later
  },
  // WIP not working
  // payment: {
  //   idProfil:() => {},
  //   datePayment:() => {},
  //   status:() => {},
  //   idStripe:() => {},
  // },
  administration: {
    title: (value) => {
      const inputValidator = { valid: false, message: "" };
      if (value.length < 2)
        inputValidator.message = i18n.t("lang.administration.global.validation.title.min");
      else if (value.length > 100) {
        inputValidator.message = i18n.t("lang.administration.global.validation.title.max");
      }
      else inputValidator.valid = true;
      return inputValidator;
    },
    message: (value) => {
      const inputValidator = { valid: false, message: "" };
      if (value.length < 2)
        inputValidator.message = i18n.t("lang.administration.global.validation.message.min");
      else if (value.length > 1000) {
        inputValidator.message = i18n.t("lang.administration.global.validation.message.max");
      } else inputValidator.valid = true;
      return inputValidator;
    },
  },
};
