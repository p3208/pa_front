import VueI18n from 'vue-i18n';
import Vue from 'vue';
import fr from './i18n/fr';

Vue.use(VueI18n);

export default new VueI18n({
  locale: 'fr',
  messages: {
    fr: {
      lang: fr
    }
  },
  silentTranslationWarn: true,
});
