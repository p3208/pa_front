export default {
  labels: {
    login: "Se connecter",
    register: {
      title: "S'inscrire",
      create: "Créer",
    },
  },
  validation: {
    firstName: {
      required: "Le prénom est requis",
      min: "Le prénom doit contenir au moins 2 caractères",
    },
    lastName: {
      required: "Le nom est requis",
      min: "Le nom doit contenir au moins 3 caractères",
    },
    email: {
      required: "L'email est requis",
      invalid: "L'email est invalide",
    },
    password: {
      required: "Le mot de passe est requis",
      min: "Le mot de passe doit contenir au moins 6 caractères",
    },
  },
};
