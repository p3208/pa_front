export default {
  labels: {
    create: {
      title: "Ajouter une mission",
      create: "Ajouter",
    },
    delete: {
      title: "Attention !",
      message: "Voulez-vous vraiment supprimer cette mission ?",
      button: "Supprimer",
    },
    show: {
      title: "Détails de la mission",
      description: "Description",
      startDate: "Date de début",
      endDate: "Date de fin",
      remuneration: "Rémunération",
    },
    edit: {
      button: "Modifier",
      save: "Sauvegarder",
      cancel: "Annuler",
    }
  },
  validation: {
    title: {
      min: "Le titre doit contenir au moins 2 caractères",
      max: "Le titre doit contenir au maximum 100 caractères",
    },
    description: {
      min: "La description doit contenir au moins 10 caractères",
      max: "La description doit contenir au maximum 1000 caractères",
    },
    startDate: {
      required: "La date de début est requise",
      invalid: "La date de début est invalide",
    },
    endDate: {
      required: "La date de fin est requise",
      invalid: "La date de fin est invalide",
    },
    remuneration: {
      min: "La rémunération ne doit pas être négatif",
    },
  }
};
