export default {
  labels: {
    delete: {
      title: "Attention !",
      message: "Voulez-vous vraiment supprimer cette utilisateur ?",
      button: "Supprimer",
    },
    edit: {
      firstName: "Prénom",
      lastName: "Nom",
      email: "Email",
      button: "Modifier",
      save: "Sauvegarder",
      cancel: "Annuler",
    }
  },
};
