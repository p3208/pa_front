const files = require.context('.', true, /\.js$/);
const locales = {};

files.keys().forEach(key => {
  if (key === './index.js') return;
  const localeKey = key.replace(/(\.\/|\.js)/g, '');

  // warning! only works if it's only directories with only files in it.
  const [ directoryName, fileName ] = localeKey.split('/');
  if (fileName === 'index') {
    locales[ directoryName ] = files(key).default;
  }
});

export default Object.freeze(locales);