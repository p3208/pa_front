export default {
  labels: {
    create: {
      title: "Créer un ticket",
      create: "Créer",
    },
    delete: {
      title: "Attention !",
      message: "Voulez-vous vraiment supprimer ce ticket ?",
      button: "Supprimer",
    },
    show: {
      title: "Détails du ticket",
      message: "Message",
    },
    edit: {
      button: "Modifier",
      save: "Sauvegarder",
      cancel: "Annuler",
    }
  },
  validation: {
    title: {
      min: "Le titre doit contenir au moins 2 caractères",
      max: "Le titre doit contenir au maximum 100 caractères",
    },
    message: {
      min: "Le message doit contenir au moins 10 caractères",
      max: "Le message doit contenir au maximum 1000 caractères",
    },
  }
};
