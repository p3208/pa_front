export default {
  labels: {
    home: 'Accueil',
    about: 'A propos',
    contact: 'Contact',
    login: 'Connexion',
    logout: 'Déconnexion',
    profil: 'Profil',
    register: 'Inscription',
    users: 'Utilisateurs',
    administrations: 'Administrations',
    missions: 'Missions',
  },
};
