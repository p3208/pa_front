import request from "../helpers/request";

const uri = "/api/back/payment";
const uriId = (id) => `${uri}/${id}`;

export default {
  get(query) {
    return request.create(uri).get(query);
  },
  getById(id) {
    return request.create(uriId(id)).get();
  },
  post(data) {
    return request.create(uri).post(data);
  },
  put(id, data) {
    return request.create(uriId(id)).put(data);
  },
  patch(id, data) {
    return request.create(uriId(id)).patch(data);
  },
  delete(id) {
    return request.create(uriId(id)).delete();
  },
};
