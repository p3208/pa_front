import request from "../helpers/request";

const uri = "/api/back";

export default {
  login(email, password) {
    return request.create(`${uri}/connexion`).post({ email, password });
  },
  register(user) {
    return request.create(`${uri}/inscription`).post(user);
  },
  logout() {}
};
