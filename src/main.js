import Vue from 'vue';
import App from './App.vue';
import router from './routes';
import i18n from './locales';

new Vue({
  el:'#app',
  render: h => h(App),
  router,
  i18n,
});