const MissionRoutes = [
  {
    name: 'mission-list',
    path: '/missions',
    component: () => import('../views/Mission/List.vue'),
  },
  {
    name: 'mission-create',
    path: '/mission/new',
    component: () => import('../views/Mission/Create.vue'),
  },
  {
    name: 'mission-show',
    path: '/mission/:id',
    component: () => import('../views/Mission/Show.vue'),
  },
];

export default MissionRoutes;
