const AdministrationRoutes = [
  {
    name: 'administration-list',
    path: '/administrations',
    component: () => import('../views/Administration/List.vue'),
  },
  {
    name: 'administration-create',
    path: '/administration/new',
    component: () => import('../views/Administration/Create.vue'),
  },
  {
    name: 'administration-show',
    path: '/administration/:id',
    component: () => import('../views/Administration/Show.vue'),
  },
];

export default AdministrationRoutes;
