const AuthentRoutes = [
  {
    name: 'login',
    path: '/login',
    component: () => import('../views/Authentification/Login.vue'),
  },
  {
    name: 'register',
    path: '/register',
    component: () => import('../views/Authentification/Register.vue'),
  },
  {
    name: 'logout',
    path: '/logout',
    component: () => import('../views/Authentification/Disconnect.vue'),
  }
];

export default AuthentRoutes;
