const UserRoutes = [
  {
    name: 'profil',
    path: '/me',
    component: import('../views/User/Profil.vue'),
  },
  {
    name: 'user-list',
    path: '/users',
    component: () => import('../views/User/List.vue'),
  },
  {
    name: 'user-show',
    path: '/user/:id',
    component: () => import('../views/User/Show.vue'),
  }
];

export default UserRoutes;
