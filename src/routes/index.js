import Vue from 'vue';
import VueRouter from 'vue-router';
import { routes } from './routes';

Vue.use(VueRouter);
let Router = new VueRouter({
  routes,
  mode: 'history'
});

// Setting up routes
export default Router;