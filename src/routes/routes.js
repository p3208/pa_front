import Home from '../views/Home.vue';

const files = require.context('.', false, /\.routes\.js$/);
const routeList = [];

files.keys().forEach(key => {
  routeList.push(...files(key).default);
});

routeList.push(
  {
    name: 'home',
    path: '/',
    component: Home,
  },
  {
    name: "about",
    path: '/about',
    component: () => import('../views/About.vue'),
  },
  {
    name: "contact",
    path: '/contact',
    component: () => import('../views/Contact.vue'),
  },
  {
    path: '*',
    redirect: '/',
  },
);

export const routes = Object.freeze(routeList);
