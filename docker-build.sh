#!/bin/bash
docker build \
  -t $TARGETED_REGISTRY_IMAGE \
  .
docker push $TARGETED_REGISTRY_IMAGE
